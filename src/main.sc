require: patterns.sc
require: helpers.js

theme: /

    state: Правила
        q!: $start
        a: Привет! Давай поиграем. Я задумываю тайное 4-значное число (без повторов!), а ты пытаешься его угадать. Если цифра верная, но не на своем месте, то это - одна корова 🐄. А если всё верно, то это - один бык 🐂! Согласен?
        buttons:
            "Да, давай!" -> /Правила/Согласен?/Да
            "Не хочу" -> /Правила/Согласен?/Нет
        go: /Правила/Согласен?

        state: Согласен?

            state: Да
                q: [$oneWord] $yes [$oneWord]
                go!: /Игра

            state: Нет
                q: [$oneWord] $no [$oneWord]
                a: Ну и ладно! Если передумаешь - скажи "давай поиграем"

    state: Игра
        q!: * (повтор*|еще|ещё|занов*|*игра*|сыгра*) *
        script:
            var numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
                result = [];
                while (result.length < 4) {
                    var number = getRandom(numbers);
                    if (result.indexOf(number) === -1) result.push(number);
                }
            $session.secretNumber = result.join("");
        a: Поехали! Не забывай, что число должно состоять из 4-х знаков
        go: Игра/Проверка

        state: Проверка
            q: *
            script:
                var reNumbers = /^\d+$/g;
                if ($parseTree.text.trim().match(reNumbers)) {
                    var attempt = $parseTree.text.trim().split('');
                    if (attempt.length == 4) {
                        if (isUnique(attempt)) {
                            var secretArray = $session.secretNumber.split('');
                            $session.cows = 0;
                            $session.bulls = 0;
                            for (var i = 0; i < attempt.length; i++) {
                                if (attempt[i] == secretArray[i]) {
                                    $session.bulls += 1;
                                } else if (secretArray.indexOf(attempt[i]) != -1) {
                                    $session.cows += 1;
                                }
                            }
                            if ($session.bulls < 4) {
                                var phrases = ["Не сдавайся!", "Уже близко.", "Пробуй ещё!", "Ты на верном пути."],
                                    randomPhrase = getRandom(phrases);
                                $reactions.answer("🐄 - {{$session.cows}}, 🐂 - {{$session.bulls}}. " + randomPhrase);
                            } else {
                                $session.secretNumber = null;
                                $reactions.answer("Поздравляю, ты выиграл! 🐂🐂🐂🐂\nЕсли хочешь повторить, напиши: повтор");
                                $reactions.transition( {value: "/Правила/Согласен?", deferred: true} );
                            }
                        } else {
                            $reactions.answer("Цифры в твоем ответе повторяются, а в моем числе нет! Попробуй ввести число без повторов");
                        }
                    } else if (attempt.length > 4) {
                        $reactions.answer("Цифр слишком много! Попробуй ввести 4 числа, например, 1234");
                    } else if (attempt.length < 4) {
                        $reactions.answer("Не добрал цифр! Для игры нужно написать 4 числа, например, 1234");
                    }
                } else {
                    $reactions.answer("Так не пойдёт, отправь мне 4 разных цифры!");
                }

    state: NoMatch || noContext = true
        event!: noMatch
        random:
            a: Прости, я тебя не понял. Если захочешь сыграть - напиши "игра"
            a: Не могу тебя понять. Когда захочется поиграть - пиши
            a: Ничего не пойму, давай лучше поиграем?
