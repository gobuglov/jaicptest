
//функция проверяет значения массива на "уникальность"
function isUnique(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr.indexOf(arr[i]) != i) return false;
    }
    return true;
}

//функция возвращает рандомный элемент массива
function getRandom(arr) {
    return arr[Math.floor(Math.random()*arr.length)];
}
